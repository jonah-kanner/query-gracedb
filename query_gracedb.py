import requests
import json
import datetime
from gwpy.time import tconvert

perpage = 500

# -- Query for production events
url = 'https://gracedb.ligo.org/api/superevents'
payload = {'count':perpage, 'format':'json', 'query':'Production'}
r = requests.get(url, params=payload)
jsoninfo = json.loads(r.content)
prodEvents = jsoninfo['superevents']


# -- For each real event, retrieve p_astro and other info
candidates = 0
for event in prodEvents:
  
  # -- Retrieve pastro
  pastro_url = event['links']['files'] + "p_astro.json"
  r = requests.get(pastro_url)
  jsoninfo = json.loads(r.content)
  event['pastro'] = jsoninfo

  # -- Retrieve analyst comments
  logurl = event['links']['logs']
  r = requests.get(logurl, params={'format':'json'})
  jsoninfo = json.loads(r.content)
  
  # -- Look for analyst comments
  commentList = []
  for logmsg in jsoninfo['log']:
    if ('analyst_comments' in logmsg['tag_names']):
        commentList.append(logmsg['comment'])
  event['commentList'] = commentList

  # -- Make formatted date string
  endutc = tconvert(event['t_end'])
  event['datestr'] = endutc.strftime('%b %d, %Y')

  # -- Check for retractions
  filesurl = event['links']['files']
  r = requests.get(filesurl, params={'format':'json'})
  jsoninfo = json.loads(r.content)
  filenames = ' '.join(jsoninfo.keys())
  
  if 'Retraction' in filenames:
    event['retract'] = True
  else: 
    event['retract'] = False
    candidates += 1

currentDT = datetime.datetime.now()
timestr = currentDT.strftime("%Y-%m-%d %H:%M") + ' Pacific Time'

# -- Add some meta data
outdict = {}
outdict['meta'] = {'updated':timestr, 'candidates':candidates}
outdict['events'] = prodEvents


# -- All done!  Save the result to a file
outfile = open('/loscdata/summary_pages/gracedb/gracedb_query.json', 'w')
outfile.write( json.dumps(outdict, indent=2))
outfile.close()

