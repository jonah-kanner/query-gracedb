import pytz
import requests
import json
import datetime
from gwpy.time import tconvert
import lxml.etree
import os
from datetime import date

perpage = 500
outdir = '/loscdata/summary_pages/detector_status/gracedb_xml'

# -- helper function to parse gcn
def process_gcn(root):
    # Read all of the VOEvent parameters from the "What" section.
    params = {elem.attrib['name']:
              elem.attrib['value']
              for elem in root.iterfind('.//Param')}
    return params

# -- Query for production events
url = 'https://gracedb.ligo.org/api/superevents'
payload = {'count':perpage, 'format':'json', 'query':'Production'}
r = requests.get(url, params=payload)
jsoninfo = json.loads(r.content)
prodEvents = jsoninfo['superevents']

# -- For each real event, retrieve p_astro and other info
candidates = 0
for event in prodEvents:
  
  # -- Identify latest VOEvent
  voevent_url = event['links']['voevents']
  payload = {'format':'json'}
  r = requests.get(voevent_url, params=payload)
  jsoninfo = json.loads(r.content)
  voevents = jsoninfo['voevents']

  # -- Filter out retractions
  good_voevents = [voe for voe in voevents if voe['voevent_type'] != 'RE']

  # -- Find retractions
  retraction_list = [voe for voe in voevents if voe['voevent_type'] == 'RE']

  #-- Get lastest VOE event
  latest_voe_url = good_voevents[-1]['links']['file']
  r = requests.get(latest_voe_url)
  root = lxml.etree.fromstring(r.content)
  voevent_dict = process_gcn(root)
  event['voevent_dict'] = voevent_dict
  event['voevent_time'] = good_voevents[-1]['created']

  #-- Construct url of skymap
  event['mapurl'] = voevent_dict['skymap_fits'][:-7]+'png' #-- hack to remove file extension and append png

  #-- Make directory tree for skymap file
  mapdir = os.path.join(outdir, 'skymaps', event['superevent_id'])
  if not os.path.isdir(mapdir): os.mkdir(mapdir)
  mapfn = os.path.join(mapdir, os.path.basename(event['mapurl']) )
  event['mapfn'] = mapfn

  # -- Check if skymap file should be downloaded
  newmap = True
  if os.path.isfile(mapfn):
      # -- Check time of map file
      mtime = os.path.getmtime(mapfn)
      mtime = datetime.datetime.fromtimestamp(mtime)
      timezone = pytz.timezone("America/Los_Angeles")  # Time zone at Caltech
      mtime = timezone.localize(mtime)

      #-- Get voevent time
      v_time = datetime.datetime.strptime(event['voevent_time'], "%Y-%m-%d %H:%M:%S %Z")
      v_time = pytz.utc.localize(v_time)
      
      delta = mtime - v_time

      if delta.total_seconds()>0:
          #-- If map file is newer than VoEvent, don't bother to download
          newmap = False

  #-- Download and save skymap file
  if newmap:
      skymap = requests.get(event['mapurl'])
      mapfile = open(mapfn, 'w')
      mapfile.write(skymap.content)
      mapfile.close()

  #-- Construct pastro dict
  pastro_dict = {}
  for key, value in voevent_dict.items():
      if key in ['BNS', 'NSBH', 'BBH', 'Terrestrial']:
          pastro_dict[key]=float(value)
  event['pastro'] = pastro_dict
  
  # -- Retrieve analyst comments
  logurl = event['links']['logs']
  r = requests.get(logurl, params={'format':'json'})
  jsoninfo = json.loads(r.content)
  
  # -- Look for analyst comments
  commentList = []
  for logmsg in jsoninfo['log']:
    if ('analyst_comments' in logmsg['tag_names']):
        commentList.append(logmsg['comment'])
  event['commentList'] = commentList

  # -- Make formatted date string
  endutc = tconvert(event['t_end'])
  event['datestr'] = endutc.strftime('%b %d, %Y')

  # -- Check for retractions
  if len(retraction_list) > 0:
    event['retract'] = True
  else: 
    event['retract'] = False
    candidates += 1


currentDT = datetime.datetime.now()
timestr = currentDT.strftime("%Y-%m-%d %H:%M") + ' Pacific Time'

# -- Add some meta data
outdict = {}
outdict['meta'] = {'updated':timestr, 'candidates':candidates}
outdict['events'] = prodEvents


# -- All done!  Save the result to a file
#outfile = open('/loscdata/summary_pages/gracedb/gracedb_query.json', 'w')
outfn = os.path.join(outdir, 'gracedb_query_xml.json')
outfile = open(outfn, 'w')
outfile.write( json.dumps(outdict, indent=2))
outfile.close()

